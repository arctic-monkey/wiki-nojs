django>=3.1
gunicorn
django-heroku

wiki>=0.7.3

# Testing
django-functest
pytest
pytest-django
pytest-cov
pytest-pythonpath