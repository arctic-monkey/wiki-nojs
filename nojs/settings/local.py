# Add your own changes here -- but do not push to remote!!
# After changing the file, from root of repository execute:
# git update-index --assume-unchanged testproject/testproject/settings/local.py
from .dev import *  # noqa @UnusedWildImport

ALLOWED_HOSTS = ["0.0.0.0", "127.0.0.1"]

INSTALLED_APPS = [
    "django.contrib.humanize", # required
    "django.contrib.auth", # required
    "django.contrib.contenttypes", # required
    "django.contrib.sessions", # required
    "django.contrib.sites", # required
    "django.contrib.messages", # required only with runserver
    # "django.contrib.staticfiles.apps.StaticFilesConfig",
    "django.contrib.admin", # required
    # "django.contrib.admindocs.apps.AdminDocsConfig",
    "sekizai", # required
    # "sorl.thumbnail",
    "django_nyt.apps.DjangoNytConfig", # required
    "wiki", # required
    # "wiki.plugins.macros.apps.MacrosConfig",
    # "wiki.plugins.help.apps.HelpConfig",
    # "wiki.plugins.links.apps.LinksConfig",
    # "wiki.plugins.images.apps.ImagesConfig",
    # "wiki.plugins.attachments.apps.AttachmentsConfig",
    # "wiki.plugins.notifications.apps.NotificationsConfig",
    # "wiki.plugins.editsection.apps.EditSectionConfig",
    # "wiki.plugins.globalhistory.apps.GlobalHistoryConfig",
    "mptt", # required only with runserver
]

# Don't forget to run:
# python manage.py migrate
# python manage.py createsuperuser
# In order to create admin credentials

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(PROJECT_DIR, "db", "test.db")
    }
}

WIKI_ANONYMOUS_WRITE = True
WIKI_ANONYMOUS_CREATE = True
WIKI_ACCOUNT_SIGNUP_ALLOWED = False