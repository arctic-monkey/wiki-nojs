from wiki.conf import settings as wiki_settings
from wiki.models import reverse

from ..base import RequireRootArticleMixin
from ..base import TestBase
from ..base import wiki_override_settings

from ..testdata.models import CustomUser

class CustomUserSignupViewTests(RequireRootArticleMixin, TestBase):
    """
    Run this test from project's root directory with arguments:
    $ pytest --ds=nojs.tests.settings
    With that settings file provided overriding of AUTH_USER_MODEL happens
    So that wiki engine treat any logged user as instance of 'testdata.CustomUser'
    Instead of 'auth.User'
    """
    @wiki_override_settings(ACCOUNT_HANDLING=True, ACCOUNT_SIGNUP_ALLOWED=True)
    def test_signup(self):
        response = self.client.post(
            wiki_settings.SIGNUP_URL,
            data={
                "password1": "wiki",
                "password2": "wiki",
                "username": "wiki",
                "email": "wiki@wiki.com",
            },
        )
        self.assertIs(CustomUser.objects.filter(email="wiki@wiki.com").exists(), True)
        self.assertRedirects(response, reverse("wiki:login"))