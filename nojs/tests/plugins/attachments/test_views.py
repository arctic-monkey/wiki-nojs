from django.contrib.auth import get_user_model

from ...base import TemplateTestCase
from ...base import wiki_override_settings

User = get_user_model()

class PluginEnabled(TemplateTestCase):

    template = """
        {% load wiki_tags %}
        {% if "wiki.plugins.attachments"|plugin_enabled %}It is enabled{% endif %}
    """

    def test_true(self):
        output = self.render({})
        self.assertIn("It is enabled", output)
