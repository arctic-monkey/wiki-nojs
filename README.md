### Run tests ###

From project's root directory (where your `manage.py` file is) run the following:

For testing core functionality, by default it will run with `nojs/settings/local.py`,
because in `pytest.ini` there is a link to `nojs.settings` *module*,
which runs `__init__.py` first:

```bash
$ pytest nojs/tests/core/
```

If you want to provide different settings, for example `/nojs/settings/base.py`,
add `--ds` argument (which means *django settings*) with path separated with dots:

```bash
$ pytest --ds=nojs.settings.base
```

For testing customization functionality:

```bash
$ pytest nojs/tests/custom/ --ds=nojs.tests.settings
```